# Hyve Coder-Decoder

To build the JAR file, run
```
mvn package -Dmaven.test.skip=true
```

To execute the result:
```
java -jar target/hyve-codec-0.0.1-SNAPSHOT.jar
```

