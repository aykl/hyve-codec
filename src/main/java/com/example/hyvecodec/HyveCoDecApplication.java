package com.example.hyvecodec;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class HyveCoDecApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(HyveCoDecApplication.class, args);
	}

	@Override
    public void run(String... args) throws IOException {
		var input = new InputStream();
		var decoder = new Decoder();
		var useTrivialEncoder = System.getenv("USE_TRIVIAL_IMPLEMENTATION");
		var encoder = "1".equals(useTrivialEncoder) ? new TrivialEncoder() : new SmartEncoder();
		input.forEach(pair -> {
			var bytes = decoder.writeDecoded(pair);
			encoder.writeEncoded(bytes);
		});
		encoder.flush();
		System.out.flush();
		System.err.flush();
	}
}

interface Encoder {
	void writeEncoded(byte[] bytes) throws IOException;
	void flush() throws IOException;
}

class TrivialEncoder implements Encoder {
	public void writeEncoded(byte[] bytes) throws IOException {
		for (var b : bytes) {
			System.err.write(new byte[]{0, b});
		}
	}

	public void flush() {
		// NO-OP
	}
}

class SmartEncoder implements Encoder {
	private List<Byte> writtenBytes = new ArrayList();
	private List<Byte> pendingBytes = new ArrayList();

	public void writeEncoded(byte[] bytes) throws IOException {
		// Let's store the bytes to see how can we group them
		for (var b : bytes) {
			pendingBytes.add(b);
		}

		// But let's not hoard them
		if (pendingBytes.size() > 255) {
			var pair = encodePair();
			var pairBytes = new byte[] { pair.p, pair.q };
			write(pairBytes);
		}
	}

	public void flush() throws IOException {
		while (pendingBytes.size() > 0) {
			var pair = encodePair();
			var pairBytes = new byte[] { pair.p, pair.q };
			write(pairBytes);
		}
	}

	private void write(byte[] bytes) throws IOException {
		System.err.write(bytes);
		for (var b : bytes) {
			writtenBytes.add(b);
		}
		writtenBytes = writtenBytes
				.stream()
				.skip(Math.max(0, writtenBytes.size() - 255))
				.collect(Collectors.toList());
	}

	private Pair encodePair() {
		// I want to find a longest sequence present in written bytes
		var seq = pendingBytes;
		while (seq.size() > 2) {
			var len = seq.size() - 1;
			seq = seq.stream().limit(len).collect(Collectors.toList());
			for (byte i = 0; i <= 255 - len; i++) {
				var wSeq = writtenBytes
						.stream()
						.skip(i)
						.limit(len)
						.collect(Collectors.toList());
				if (wSeq.size() < len) break;
				for (var j = 0; j < len; j++) {
					if (!wSeq.get(j).equals(seq.get(j))) break;
				}
				pendingBytes = pendingBytes
						.stream()
						.skip(len)
						.collect(Collectors.toList());
				return new Pair((byte) (writtenBytes.size() - i), (byte) len);
			}
		}
		var q = pendingBytes.get(0);
		pendingBytes = pendingBytes
				.stream()
				.skip(1)
				.collect(Collectors.toList());
		return new Pair((byte) 0, q);
	}
}

class Decoder {
	// We need to store up to 255 bytes for decoding,
	// but I'm not going to implement
	// a proper circular buffer now
	private List<Byte> writtenBytes = new ArrayList();

	public byte[] writeDecoded(Pair p) throws IOException {
		var bytes = this.decode(p);
		System.out.write(bytes);
		for (var i = bytes.length; i > 0; i--) {
			writtenBytes.add(bytes[i - 1]);
		}
		writtenBytes = writtenBytes
				.stream()
				.skip(Math.max(0, writtenBytes.size() - 255))
				.collect(Collectors.toList());
		return bytes;
	}

	private byte[] decode(Pair pair) {
		var p = pair.p;
		var q = pair.q;
		var BAD_PAIR = new byte[] { 0x3F };

		if (q == null) return BAD_PAIR;
		if (p == 0) return new byte[] { q };
		if (q > p) return BAD_PAIR;
		if (q == 0) return BAD_PAIR;
		if (p > writtenBytes.size()) return BAD_PAIR;
		var bytesToTake = writtenBytes
				.stream()
				.skip(Math.max(0, writtenBytes.size() - p))
				.limit(q)
				.toArray(Byte[]::new);
		var result = new byte[bytesToTake.length];
		for (int i = 0; i < bytesToTake.length; i++ ) {
			result[i] = bytesToTake[i];
		}
		return result;
	}
}

class Pair {
	public byte p;
	// What if we have an incomplete byte pair?
	public Byte q;

	public Pair(byte p, Byte q) {
		this.p = p;
		this.q = q;
	}
}

class InputStream {
	public void forEach(ThrowingConsumer<Pair, IOException> action) throws IOException {
		var buffer = new byte[10000];
		while (true) {
			int bytesRead = System.in.read(buffer);
			if (bytesRead == -1) {
				return;
			}
			for (var i = 0; i < bytesRead; i += 2) {
				var p = buffer[i];
				var q  = bytesRead > i + 1 ? buffer[i + 1] : null;
				var pair = new Pair(p, q);
				action.accept(pair);
			}
		}
	}
}

@FunctionalInterface
interface ThrowingConsumer<T, E extends Exception> {
	void accept(T t) throws E;
}
